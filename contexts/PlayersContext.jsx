import React, { createContext, useState, useEffect } from "react";
import Storage from "react-native-storage";
import AsyncStorage from "@react-native-async-storage/async-storage";

export const PlayersContext = createContext();

const storage = new Storage({
  size: 1000,
  storageBackend: AsyncStorage,
  defaultExpires: null,
  enableCache: true,
  sync: {},
});

export const PlayersProvider = ({ children }) => {
  const [players, setPlayers] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    storage
      .load({ key: "players" })
      .then((data) => {
        setPlayers(data);
        setLoading(false);
      })
      .catch(() => {});
  }, []);

  useEffect(() => {
    if (loading) {
      return;
    }
    console.log("Saving players to storage")
    savePlayersToStorage(players);
  }, [players]);

  function generateId() {
    let timestamp = new Date().getTime();
    return timestamp + Math.floor(Math.random() * 1000);
  }

  function savePlayersToStorage(players) {
    storage.save({
      key: "players",
      data: players,
    });
  }

  function createPlayer(name) {
    const player = { id: generateId(), name, score: 0 };
    setPlayers([...players, player]);
  }

  function updatePlayer(id, updatedPlayer) {
    setPlayers(
      players.map((player) => (player.id === id ? updatedPlayer : player))
    );
  }

  function removePlayer(id) {
    setPlayers(players.filter((player) => player.id !== id));
  }

  function modifyPlayerName(id, newName) {
    const player = players.find((player) => player.id === id);
    if (player) {
      player.name = newName;
      setPlayers([...players]);
    }
  }

  function addScore(id, score) {
    const player = players.find((player) => player.id === id);
    if (player) {
      let playerScore = parseInt(player.score);
      if (isNaN(playerScore)) {
        playerScore = 0;
      }
      player.score = playerScore + score;
      setPlayers([...players]);
    }
  }

  const playersContextValue = {
    players,
    createPlayer,
    updatePlayer,
    removePlayer,
    modifyPlayerName,
    addScore,
  };

  return (
    <PlayersContext.Provider value={playersContextValue}>
      {children}
    </PlayersContext.Provider>
  );
};

export const usePlayers = () => {
  const context = React.useContext(PlayersContext);
  if (!context) {
    throw new Error("usePlayers must be used within a PlayersProvider");
  }
  return context;
};
