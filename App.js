import { NavigationContainer } from "@react-navigation/native";
import SelectNamesScreen from "./screens/SelectNamesScreen";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { PaperProvider } from "react-native-paper";
import { PlayersProvider } from "./contexts/PlayersContext";
import ScoresScreen from "./screens/ScoresScreen";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const Tab = createBottomTabNavigator();

export default function App() {
  return (
    <PaperProvider>
      <PlayersProvider>
        <NavigationContainer>
          <Tab.Navigator
            screenOptions={({ route }) => ({
              tabBarIcon: ({ color, size }) => {
                let iconName = "";
                if (route.name === "Players") {
                  iconName = "account-multiple";
                } else if (route.name === "Score") {
                  iconName = "trophy";
                }
                return <MaterialCommunityIcons name={iconName} size={size} color={color} />;
              },
            })}
          >
            <Tab.Screen name="Players" component={SelectNamesScreen} />
            <Tab.Screen name="Score" component={ScoresScreen} />
          </Tab.Navigator>
        </NavigationContainer>
      </PlayersProvider>
    </PaperProvider>
  );
}
