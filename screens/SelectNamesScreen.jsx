import { View, StyleSheet, FlatList, Alert } from 'react-native'
import { usePlayers } from '../contexts/PlayersContext'
import { Button, TextInput, Text } from "react-native-paper";
import { useState } from 'react';

export default function SelectNamesScreen() {
  const { createPlayer, removePlayer, players, modifyPlayerName } = usePlayers();
  const [nextName, setNextName] = useState('');

  function handleOnPressAddPlayer() {
    if (!nextName) {
      Alert.alert('Invalid name', 'Please enter a valid name');
      return;
    };
    createPlayer(nextName);
    setNextName('');
  }

  return (
    <View style={styles.container}>
      {players.length === 0 && <Text>No players yet</Text>}
      <FlatList
        data={players}
        keyExtractor={(item) => item.id}
        renderItem={({ item }) => (
          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
            <TextInput
              value={item.name}
              onChangeText={(text) => modifyPlayerName(item.id, text)}
              style={{ flex: 1 }}
            />
            <Button onPress={() => removePlayer(item.id)}>Remove</Button>
          </View>
        )}
        ItemSeparatorComponent={() => <View style={{height: 10}} />}
      />
      <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
        <TextInput
          value={nextName}
          onChangeText={setNextName}
          style={{ flex: 1 }}
        />
        <Button onPress={handleOnPressAddPlayer}>Add</Button>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
  }
});