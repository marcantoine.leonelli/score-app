import React from "react";
import { View, StyleSheet, Alert } from "react-native";
import { usePlayers } from "../contexts/PlayersContext";
import {
  Table,
  TableWrapper,
  Row,
  Rows,
  Col,
  Cols,
  Cell,
} from "react-native-table-component";
import { Button, TextInput, Text } from "react-native-paper";
import Dialog from "react-native-dialog";

const ScoresScreen = () => {
  const { players, addScore } = usePlayers();
  const [dialogVisible, setDialogVisible] = React.useState(false);
  const [scoreToAdd, setScoreToAdd] = React.useState(0);
  const [playerToAddScore, setPlayerToAddScore] = React.useState(null);

  function getRank(player) {
    return players.filter((p) => p.score > player.score).length + 1;
  }

  function handleOnPressAddScore(id) {
    setDialogVisible(true);
    setPlayerToAddScore(id);
  }

  function submitScore() {
    if (!playerToAddScore) {
      return;
    }
    let score = parseInt(scoreToAdd);
    if (isNaN(score)) {
      Alert.alert("Invalid score", "Please enter a valid score");
      return;
    }
    addScore(playerToAddScore, score);
    setScoreToAdd(0);
    setDialogVisible(false);
  }

  function cancelAddScore() {
    setDialogVisible(false);
    setPlayerToAddScore(null);
    setScoreToAdd(0);
  }

  return (
    <View style={styles.container}>
      <Table borderStyle={{ borderWidth: 2, borderColor: "#c8e1ff" }}>
        <Row
          data={[
            <Text style={styles.dataHeader}>Name</Text>,
            <Text style={styles.dataHeader}>Score</Text>,
            <Text style={styles.dataHeader}>Rank</Text>,
            <Text style={styles.dataHeader}>Add Score</Text>,
          ]}
          style={{ height: 40, backgroundColor: "#f1f8ff" }}
        />
        <Rows
          data={players.map((player) => [
            <Text style={styles.dataRow}>{player.name}</Text>,
            <Text style={styles.dataRow}>{player.score}</Text>,
            <Text style={styles.dataRow}>{getRank(player)}</Text>,
            <Button onPress={() => handleOnPressAddScore(player.id)}>Add</Button>,
          ])}
        />
      </Table>
      <Dialog.Container visible={dialogVisible}>
        <Dialog.Title>Enter score</Dialog.Title>
        <Dialog.Input
          label="Score"
          value={scoreToAdd}
          onChangeText={setScoreToAdd}
          keyboardType="numeric"
        />
        <Dialog.Button label="Cancel" onPress={cancelAddScore} />
        <Dialog.Button label="Add" onPress={submitScore} />
      </Dialog.Container>
    </View>
  );
};

const styles = StyleSheet.create({
  container: { flex: 1, padding: 10 },
  dataRow: { textAlign: "center" },
  dataHeader: { textAlign: "center", fontWeight: "bold" },
});

export default ScoresScreen;
